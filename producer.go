package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"sync"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/b5y/go-producer/src/api"
	"gitlab.com/b5y/go-producer/src/config"
	"gitlab.com/b5y/go-producer/src/utils"
)

func init() {
	log.SetFormatter(&log.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		FullTimestamp:   true})
	var err error
	configPath := flag.String("c", "", "config file path")
	flag.Usage = func() {
		fmt.Printf("Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()
	if config.Config, err = config.InitConfig(*configPath); err != nil {
		log.Warnf("Cannot initialize config file: ", err.Error())
		panic(err)
	}
	utils.ConfigureStdLogger()
}

func main() {
	filePath, err := os.Open(config.Config.FilePath)
	defer filePath.Close()
	if err != nil {
		log.Error("Cannot read file: ", err.Error())
	}
	reader := csv.NewReader(bufio.NewReader(filePath))
	ch := make(chan []string, 100)
	var wg sync.WaitGroup
	i := 0
	for {
		i++
		record, err := reader.Read()
		if err != nil {
			log.Error("Cannot read CSV file", err.Error())
		}
		wg.Add(1)
		go func(r []string, i int) {
			defer wg.Done()
			api.Messanger(r)
			ch <- r
		}(record, i)
	}
}
