package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/b5y/go-producer/src/config"
)

// PostData is used to send data to consumer
type PostData struct {
	Email    string `json:"Email"`
	FullName string `json:"FullName"`
}

// Messanger is used to message about every raw data
func Messanger(data []string) {
	client := &http.Client{Timeout: time.Duration(config.Config.TimeOut) * time.Second}
	var send *PostData
	if strings.Contains(data[0], "@") {
		send = &PostData{
			Email:    data[0],
			FullName: data[1],
		}
	} else {
		send = &PostData{
			Email:    data[1],
			FullName: data[0],
		}
	}
	buf := new(bytes.Buffer)
	err := json.NewEncoder(buf).Encode(send)
	if err != nil {
		log.Error("Cannot encode data", err.Error())
	}
	req, err := http.NewRequest("POST", config.Config.URL, buf)
	if err != nil {
		log.Error("Cannot post data: ", err.Error())
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil || resp.StatusCode >= 400 {
		log.Warnf("Incorrect response: ", err.Error())
		log.Info("Error message: ", resp.StatusCode)
	}
	defer resp.Body.Close()
}
