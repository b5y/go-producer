package config

import (
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestYamlLoad(t *testing.T) {
	var getAPI ProducerConfig
	configData := "file_path: 'http://000.000.000.040:00100'"
	confFile, err := ioutil.TempFile(os.TempDir(), "temp")
	defer os.Remove(confFile.Name())
	if err != nil {
		t.Fatal(err)
	}
	_, err = confFile.Write([]byte(configData))
	if err != nil {
		log.Fatal("Write failed")
	}
	getAPI.YamlLoad(confFile.Name())
	result := getAPI.FilePath
	expRes := "http://000.000.000.040:00100"
	if expRes != result {
		t.Error("Expected value: ", expRes,
			"Actial value: ", result)
	}
}

func TestInitConfig(t *testing.T) {
	var getAPI *ProducerConfig
	configData := "file_path: 'http://000.000.000.040:00100'"
	confFile, err := ioutil.TempFile(os.TempDir(), "temp")
	defer os.Remove(confFile.Name())
	if err != nil {
		t.Fatal(err)
	}
	_, err = confFile.Write([]byte(configData))
	if err != nil {
		log.Fatal("Write failed")
	}
	getAPI, _ = InitConfig(confFile.Name())
	result := getAPI.FilePath
	expRes := "http://000.000.000.040:00100"
	if expRes != result {
		t.Error("Expected value: ", expRes,
			"Actial value: ", result)
	}
}
