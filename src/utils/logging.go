package utils

import (
	"log/syslog"
	"os"

	"git.selectel.org/vpc/biscuit/src/config"
	log "github.com/Sirupsen/logrus"
	"github.com/Sirupsen/logrus/hooks/syslog"
	"github.com/rifflock/lfshook"
)

// ConfigureStdLogger is used to configure standard logger
func ConfigureStdLogger() {
	log.StandardLogger().Out = os.Stdout

	path := config.Config.Log.File

	if path != "" {
		configureFileLogger(path)
	}

	if config.Config.Log.SyslogEnabled {
		hook, err := logrus_syslog.NewSyslogHook("", "", syslog.LOG_WARNING, "producer")

		if err == nil {
			log.StandardLogger().Hooks.Add(hook)
		} else {
			log.Warnf("Failed to configure syslog hook: %v", err)
		}
	}
}

func configureFileLogger(path string) {
	if path == "" {
		return
	}
	log.StandardLogger().Hooks.Add(
		lfshook.NewHook(
			lfshook.PathMap{
				log.DebugLevel: path,
				log.InfoLevel:  path,
				log.ErrorLevel: path,
				log.FatalLevel: path,
				log.WarnLevel:  path,
			},
		),
	)
	log.Infof("Configured logging to %v.", path)
}
